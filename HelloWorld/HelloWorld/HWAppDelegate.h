//
//  HWAppDelegate.h
//  HelloWorld
//
//  Created by Scott Hammer on 12/11/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
