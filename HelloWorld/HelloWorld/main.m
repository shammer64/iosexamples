//
//  main.m
//  HelloWorld
//
//  Created by Scott Hammer on 12/11/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HWAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HWAppDelegate class]));
    }
}
