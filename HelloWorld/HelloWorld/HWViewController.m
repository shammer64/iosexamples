//
//  HWViewController.m
//  HelloWorld
//
//  Created by Scott Hammer on 12/11/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import "HWViewController.h"

@interface HWViewController ()
- (IBAction)changeGreeting:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end

@implementation HWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeGreeting:(id)sender {
    self.userName = self.nameTextField.text;
    NSString *nameString = self.userName;
    if ([nameString length] == 0) {
        nameString = @"World";
    }
    NSString *greetingString = [[NSString alloc] initWithFormat:@"Hello, %@!", nameString];
    self.nameLabel.text = greetingString;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField == self.nameTextField) {
        [theTextField resignFirstResponder];
    }
    return YES;
}

@end
