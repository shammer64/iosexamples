//
//  HWViewController.h
//  HelloWorld
//
//  Created by Scott Hammer on 12/11/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWViewController : UIViewController <UITextFieldDelegate>
@property (copy, nonatomic) NSString *userName;

@end
