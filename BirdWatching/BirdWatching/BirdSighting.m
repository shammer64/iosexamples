//
//  BirdSighting.m
//  BirdWatching
//
//  Created by Scott Hammer on 12/12/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import "BirdSighting.h"

@implementation BirdSighting

-(id)initWithName:(NSString *)name location:(NSString *)location date:(NSDate *)date {
    self = [super init];
    if (self) {
        _name = name;
        _location = location;
        _date = date;
    }
    return self;
}

@end
