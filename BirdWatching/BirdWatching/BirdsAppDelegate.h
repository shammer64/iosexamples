//
//  BirdsAppDelegate.h
//  BirdWatching
//
//  Created by Scott Hammer on 12/12/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BirdsAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
