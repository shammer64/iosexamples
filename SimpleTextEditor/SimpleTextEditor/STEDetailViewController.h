//
//  STEDetailViewController.h
//  SimpleTextEditor
//
//  Created by Scott Hammer on 12/16/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STESimpleTextDocument.h"

@interface STEDetailViewController : UIViewController <UISplitViewControllerDelegate, STESimpleTextDocumentDelegate>
@property (strong, nonatomic) NSURL *detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (strong, nonatomic) IBOutlet UITextView *textView;

@end
