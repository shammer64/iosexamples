//
//  STEMasterViewController.h
//  SimpleTextEditor
//
//  Created by Scott Hammer on 12/16/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import <UIKit/UIKit.h>

@class STEDetailViewController;

@interface STEMasterViewController : UITableViewController

@property (strong, nonatomic) STEDetailViewController *detailViewController;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addButton;

@end
