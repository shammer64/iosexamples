//
//  STEAppDelegate.h
//  SimpleTextEditor
//
//  Created by Scott Hammer on 12/16/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STEAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
