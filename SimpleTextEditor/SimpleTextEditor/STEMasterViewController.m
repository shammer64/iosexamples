//
//  STEMasterViewController.m
//  SimpleTextEditor
//
//  Created by Scott Hammer on 12/16/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import "STEMasterViewController.h"
#import "STEDetailViewController.h"
#import "STESimpleTextDocument.h"

NSString *STEDocsDirectory = @"Documents";
NSString *STEDocFilenameExtension = @"stedoc";
NSString *STEUbiquityContainerId = @"com.shammer.SimpleTextEditor";

NSString *STEDisplayDetailSeque = @"DisplayDetailSeque";
NSString *STEDocEntryCell = @"DocumentEntryCell";

@interface STEMasterViewController () {
    NSMutableArray *documents;
}
@end

@implementation STEMasterViewController

- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    }
    if (!documents) {
        documents = [[NSMutableArray alloc] init];
    }
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (STEDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
- (void)insertNewObject:(id)sender
{
    [documents insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}
*/

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [documents count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *newCell = [tableView dequeueReusableCellWithIdentifier:STEDocEntryCell];

    if (!newCell) {
        newCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:STEDocEntryCell];
    }
    
    if (!newCell) {
        return nil;
    }
    
    NSURL *fileURL = [documents objectAtIndex:[indexPath row]];
    newCell.textLabel.text = [[fileURL lastPathComponent] stringByDeletingPathExtension];
    return newCell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSURL *fileURL = [documents objectAtIndex:[indexPath row]];
        
        // Don't use file coordinators on app's main queue
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSFileCoordinator *fc = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
            [fc coordinateWritingItemAtURL:fileURL
                                   options:NSFileCoordinatorWritingForDeleting
                                     error:nil
                                byAccessor:^(NSURL *newURL) {
                                    NSFileManager *fm = [NSFileManager defaultManager];
                                    [fm removeItemAtURL:newURL error:nil];
                                }
             ];
            
        });
        [documents removeObjectAtIndex:[indexPath row]];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        NSURL *fileURL = documents[indexPath.row];
        self.detailViewController.detailItem = fileURL;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:STEDisplayDetailSeque]) {
        STEDetailViewController *destVC = (STEDetailViewController *) [segue destinationViewController];
        NSIndexPath *cellPath = [self.tableView indexPathForSelectedRow];
        UITableViewCell *theCell = [self.tableView cellForRowAtIndexPath:cellPath];
        NSURL *fileURL = [documents objectAtIndex:[cellPath row]];
        
        // Assign URL to detail view controller and set title
        destVC.detailItem = fileURL;
        destVC.navigationItem.title = theCell.textLabel.text;
    }
}

- (NSString *)newUntitledDocumentName {
    NSInteger docCount = 1;
    NSString *newDocName = nil;
    
    BOOL done = NO;
    while (!done) {
        newDocName = [NSString stringWithFormat:@"Note %d.%@", docCount, STEDocFilenameExtension];
        BOOL nameExists = NO;
        for (NSURL * aURL in documents) {
            if ([[aURL lastPathComponent] isEqualToString:newDocName]) {
                docCount++;
                nameExists = YES;
                break;
            }
        }
        
        if (!nameExists) {
            done = YES;
        }
    }
    return newDocName;
}

- (IBAction)addDocument:(id)sender {
    // Disable add button while creating document
    self.addButton.enabled = NO;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       // Create new URL object on a background queue
        NSFileManager *fm = [NSFileManager defaultManager];
        NSURL *newDocumentURL = [fm URLForUbiquityContainerIdentifier:STEUbiquityContainerId];
        newDocumentURL = [newDocumentURL URLByAppendingPathComponent:STEDocsDirectory isDirectory:YES];
        newDocumentURL = [newDocumentURL URLByAppendingPathComponent:[self newUntitledDocumentName]];
        
        // Perform the remaining tasks on the main queue
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the data structures and table
            [documents addObject:newDocumentURL];
            
            // Update the table
            NSIndexPath *newCellIndexPath = [NSIndexPath indexPathForRow:([documents count] - 1) inSection:0];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newCellIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView selectRowAtIndexPath:newCellIndexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
            // Segue to the detail view controller to begin editing
            UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:newCellIndexPath];
            [self performSegueWithIdentifier:STEDisplayDetailSeque sender:selectedCell];
            
            // Reenable the add button
            self.addButton.enabled = YES;
        });
    });
}

@end
