//
//  STESimpleTextDocument.m
//  SimpleTextEditor
//
//  Created by Scott Hammer on 12/16/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import "STESimpleTextDocument.h"

@implementation STESimpleTextDocument
@synthesize documentText = _documentText;
@synthesize delegate = _delegate;

- (void)setDocumentText:(NSString *)newText {
    NSString * oldText = _documentText;
    _documentText = [newText copy];
    
    // Register undo operation
    [self.undoManager setActionName:@"Text Change"];
    [self.undoManager registerUndoWithTarget:self
                                    selector:@selector(setDocumentText:)
                                      object:oldText];
}

- (id)contentsForType:(NSString *)typeName error:(NSError *__autoreleasing *)outError {
    if (!self.documentText) {
        self.documentText = @"";
    }
    NSData *docData = [self.documentText dataUsingEncoding:NSUTF8StringEncoding];
    return docData;
}

-(BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName error:(NSError *__autoreleasing *)outError {
    if ([contents length] > 0) {
        self.documentText = [[NSString alloc] initWithData:contents
                                                  encoding:NSUTF8StringEncoding];
    } else {
        self.documentText = @"";
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(documentContentsDidChange:)]) {
        [self.delegate documentContentsDidChange:self];
    }
    return YES;
}

@end
