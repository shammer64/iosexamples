//
//  STESimpleTextDocument.h
//  SimpleTextEditor
//
//  Created by Scott Hammer on 12/16/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol STESimpleTextDocumentDelegate;

@interface STESimpleTextDocument : UIDocument
@property (copy, nonatomic) NSString *documentText;
@property (weak, nonatomic) id<STESimpleTextDocumentDelegate> delegate;
@end

@protocol STESimpleTextDocumentDelegate <NSObject>

@optional
- (void)documentContentsDidChange:(STESimpleTextDocument *)document;

@end
