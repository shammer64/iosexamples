//
//  main.m
//  SimpleTextEditor
//
//  Created by Scott Hammer on 12/16/12.
//  Copyright (c) 2012 Scott Hammer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "STEAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([STEAppDelegate class]));
    }
}
